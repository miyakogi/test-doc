# GitLabを試してみます

GitLabで、

* ドキュメント（mkdocs）のリポジトリをgit管理して
* 変更がpushされたらビルドして
* GitLab pagesに表示する

というのをやってみました。

リポジトリは[ここ](https://gitlab.com/miyakogi/test-doc)です。

Webから編集してみます